# ML07 Models
This repo contains several 3d models I created for the Silverstone ML07 PC case. Both STL models and FreeCAD Project files should be included for everything. I printed all parts on a Prusa MK3s+ at .15 layer height. All parts tested and working.

# Fan Shroud
## Noctua a12x25
This fan shroud fit for my Thermalright AXP120-X67 cooler. I had swapped the fan for a full Noctua A12x25 and the fan shroud sits against it perfectly. It improved temperatures by like 1 degree at idle, and massively improved the amount of time it takes to bring the system up to peak temperature, giving me like 45 to 60 more seconds of full PBO2 (AMD) before the processor thermal throttles.

## Phantek T30
The strongest configuration I've tried yet. The shroud is meant to go with the rubber noctua mount things. The little disk that sits in between both parts is consumed by the gap on the underside of the shroud. Both the angle and the base surface were adjusted for this.

# Handle
The clip goes in through the vent slots on the protruding middle section at the top. The mount slides on the clip and friction fits. Some glue may be used to hold the mount in place. The barrel goes into the first mount before you slide the second one on.
